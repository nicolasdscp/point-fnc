#include "macro.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

VOID display(INT *tab, INT length) {
  for (INT i = 0; i < length; i++) {
    printf("%d\t", tab[i]);
  }
  printf("\n");
}

VOID displayStr(STRING strs[], INT length) {
  for (INT i = 0; i < length; i++) {
    printf("%s\t", strs[i]);
  }
  printf("\n");
}

INT compareToInt(INT inta, INT intb) { return inta - intb; }
INT compareToFloat(FLOAT flta, FLOAT fltb) { return flta - fltb; }
INT compareToChar(CHAR chra, CHAR chrb) { return chra - chrb; }

VOID permuteInt(INT *tab, INT i, INT j) {
  INT tmp = tab[j];
  tab[j] = tab[i];
  tab[i] = tmp;
}
VOID permuteFloat(FLOAT *tab, INT i, INT j) {
  FLOAT tmp = tab[j];
  tab[j] = tab[i];
  tab[i] = tmp;
}
VOID permuteStr(STRING*tab, INT i, INT j) {
  STRING tmp = tab[j];
  tab[j] = tab[i];
  tab[i] = tmp;
}

VOID sort(INT *tab, INT length, INT (*compare)(INT, INT),
          VOID (*permute)(INT *, INT, INT)) {
  for (INT i = 0; i < length; i++) {
    for (INT j = 0; j < length - i - 1; j++) {
      if (compare(tab[j], tab[j + 1]) > 0) {
        permute(tab, j, j + 1);
      }
    }
  }
}

VOID sortFloat(FLOAT *tab, INT length, INT (*compare)(FLOAT, FLOAT),
               VOID (*permute)(FLOAT *, INT, INT)) {
  for (INT i = 0; i < length; i++) {
    for (INT j = 0; j < length - i - 1; j++) {
      if (compare(tab[j], tab[j + 1]) > 0) {
        permute(tab, j, j + 1);
      }
    }
  }
}

VOID sortChar(STRING*tab, INT length, INT FUNC(cmp)(const char*, const char*), VOID FUNC(perm)(STRING*, INT, INT)) {
  for (INT i = 0; i < length; i++) {
    for (INT j = 0; j < length - i - 1; j++) {
      if (cmp(tab[j], tab[j + 1]) > 0) {
        perm(tab, j, j + 1);
      }
    }
  }
}

INT main(INT argc, char const *argv[]) {
    INT length = 10;
    INT tab[10] = {0, 5, 51, 5, 4, 8, 60, 55, 4, -6};

    sort(tab, length, compareToInt, permuteInt);
    display(tab, length);

    STRING str[] = {
        "test", "sort", "compare", "display"
    };
    sortChar(str, 4, strcmp, permuteStr);
    displayStr(str, 4);

    return 0;
}