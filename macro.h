/* 
   File: macros.h
   Author: Nicolas Descamps
   Date: 2020/05/17
   Desc: Macros helper file
*/
#ifndef MOD_LIB
    #define MOD_LIB

    #include <stdlib.h> 

    #ifndef NULL 
        #define NULL (void*)0
    #endif

    #define NEW(type) ((type*)malloc(sizeof(type)))
    #define DELETE(ptr) free(ptr)

    #define FUNC(fnc) (*fnc)

    #define GETV(ptr, type) (*(type*)ptr)    // Get pointer value casted in given type

    typedef void VOID;

    typedef signed char INT8;
    typedef signed short INT16;
    typedef signed int INT32;
    typedef signed long INT64;

    typedef char CHAR;
    typedef short SHORT;
    typedef int INT;
    typedef float FLOAT;
    typedef long LONG;
    typedef double DOUBLE;

    typedef unsigned char UINT8;
    typedef unsigned short UINT16;
    typedef unsigned int UINT32;
    typedef unsigned long UINT64;

    typedef unsigned char UCHAR;
    typedef unsigned short USHORT;
    typedef unsigned int UINT;
    typedef unsigned long ULONG;

    typedef CHAR* STRING;
    typedef UCHAR BYTE;

    typedef int BOOL;
    typedef BYTE BOOLEAN;

    #ifndef TRUE
        #define TRUE 1
    #endif

    #ifndef FALSE
        #define FALSE 0
    #endif
#endif
