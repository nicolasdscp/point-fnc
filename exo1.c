#include <stdio.h>
#include "macro.h"

#define ARRAY_DEF {1,2,3,4,5}
#define ARRAY_SIZE 5

/* Multiply int by 2 */
INT fois_deux(INT a) {
    return 2*a;
}

INT divise_deux(INT a) {
    return a/2;
}

VOID apply(INT tab[], UINT size, INT FUNC(fnc) (INT)) {
    for (UINT i = 0; i < size; i++) {
        tab[i] = fnc(tab[i]);
    }
}

INT main(INT argc, STRING argv[]) {
    /* Definition du tableau */
    INT lolArray[] = ARRAY_DEF;
    /* On applique la fnc fois_deux */
    apply(lolArray, ARRAY_SIZE, fois_deux);

    /* Test fois_deux */
    for (UINT i = 0; i < ARRAY_SIZE; i++) {
        printf("%d * 2 = %d\n", i+1, lolArray[i]);
    }

    /* On applique la fnc divise_deux */
    apply(lolArray, ARRAY_SIZE, divise_deux);

    /* Test divise_deux */
    for (UINT i = 0; i < ARRAY_SIZE; i++) {
        printf("%d / 2 = %d\n", (i+1)*2, lolArray[i]);
    }

    return 0;
}