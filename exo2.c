#include <stdio.h>
#include "macro.h"

#define ARRAY_DEF {1,2,3,4,5}
#define ARRAY_SIZE 5

BOOLEAN estPaire(INT n) {
    return !(n%2);
}

BOOLEAN estImpaire(INT n) {
    return n%2;
}

BOOLEAN estPremier(INT n) {
    if (n == 1 || n <= 0) return FALSE;
    for (INT i = n - 1; i > 1; i--) {
        if (n % i == 0) {
            return FALSE;
        }
    }
    return TRUE;
}

BOOLEAN estNegatif(INT n) {
    return n<0;
} 

VOID delWithCd(INT array[], UINT size, BOOLEAN FUNC(test) (INT), INT** retArr, UINT* retSize) {
    /* Pre test  */
    UINT toDelCount = 0;
    for (UINT i = 0; i < size; i++) {
        if (test(array[i])) toDelCount++;
    }
    UINT sizeNewTab = size - toDelCount;
    INT* retVal = (INT*)malloc(sizeof(INT) * (sizeNewTab));

    for (UINT i = 0, j = 0; i < size; i++) {
        if (!test(array[i])) {
            retVal[j] = array[i];
            j++;
        }
    }

    /* Return result */
    *retSize = sizeNewTab;
    *retArr = retVal;
}

INT main(INT argc, STRING argv[]) {
    /* Definition du tableau */
    INT lolArray[] = ARRAY_DEF;
    INT* paireTab = NULL;
    INT* impaireTab = NULL;
    INT* premierTab = NULL;
    INT* negatifTab = NULL;
    UINT paireTabSize = 0;
    UINT impaireTabSize = 0;
    UINT premierTabSize = 0;
    UINT negatifTabSize = 0;

    /* On applique la fnc fois_deux */
    delWithCd(lolArray, ARRAY_SIZE, estPaire, &paireTab, &paireTabSize);
    delWithCd(lolArray, ARRAY_SIZE, estImpaire, &impaireTab, &impaireTabSize);
    delWithCd(lolArray, ARRAY_SIZE, estPremier, &premierTab, &premierTabSize);
    delWithCd(lolArray, ARRAY_SIZE, estNegatif, &negatifTab, &negatifTabSize);

    printf("///////////\n");
    for (INT i = 0; i < paireTabSize; i++) {
        printf("%d\n", paireTab[i]);
    }

    printf("///////////\n");
    for (INT i = 0; i < impaireTabSize; i++) {
        printf("%d\n", impaireTab[i]);
    }

    printf("///////////\n");
    for (INT i = 0; i < premierTabSize; i++) {
        printf("%d\n", premierTab[i]);
    }

    printf("///////////\n");
    for (INT i = 0; i < negatifTabSize; i++) {
        printf("%d\n", negatifTab[i]);
    }

    DELETE(paireTab);
    DELETE(impaireTab);
    DELETE(premierTab);
    DELETE(negatifTab);

    return 0;
}